﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using MihaZupan;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;

namespace BotManager.Model
{
    class TelegramMessageClient
    {
        private readonly MainWindow _mainWindow;
        private readonly TelegramBotClient _bot;
        public ObservableCollection<MessageLog> MessageLogs { get; set; }
        public ObservableCollection<Contact> ContactsList { get; set; }

        private const string HystoryPath = @"history.json";
        private const string ContactsPath = @"contacts.json";
        private const string SettingsPath = @"settings.txt";
        public TelegramMessageClient(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;

            var history = DataLoader.ImportJson<MessageLog>(HystoryPath);
            MessageLogs = new ObservableCollection<MessageLog>(history);

            var contacts = DataLoader.ImportJson<Contact>(ContactsPath);
            ContactsList = new ObservableCollection<Contact>(contacts);


            if (!File.Exists(SettingsPath))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Загрузить токен для Telegram бота";
                openFileDialog.DefaultExt = ".txt";
                openFileDialog.Filter = "Text documents (.txt)|*.txt";
                if (openFileDialog.ShowDialog() != true)
                {
                    Application.Current.Shutdown();
                    return;
                }

                File.WriteAllText(SettingsPath, openFileDialog.FileName);
            }

            string tokenPath = File.ReadAllText(SettingsPath);

            HttpToSocks5Proxy socks5Proxy = new HttpToSocks5Proxy("139.59.137.156", 1080);

            HttpClientHandler httpClientHandler = new HttpClientHandler(){Proxy = socks5Proxy};
            HttpClient httpClient = new HttpClient(httpClientHandler);

            _bot = new TelegramBotClient(File.ReadAllText(tokenPath), httpClient);
            _bot.OnMessage += OnMessage;

            _bot.StartReceiving();
        }

        private bool IsContainsContact(Contact contact)
        {
            foreach (var itemContact in ContactsList)
            {
                if (itemContact.Id == contact.Id) return true;
            }

            return false;
        }

        private void OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            string Message;

            switch (e.Message.Type)
            {
                case MessageType.Text:
                    Message = e.Message.Text;
                    break;
                case MessageType.Photo:
                    Message = $"Photo";
                    break;
                case MessageType.Audio:
                    Message = $"Audio";
                    break;
                case MessageType.Document:
                    Message = $"File: {e.Message.Document.FileName}";
                    break;
                case MessageType.Sticker:
                    Message = $"Sticker";
                    break;
                default:
                    return;
            }

            _mainWindow.Dispatcher.Invoke(() =>
            {
                Contact contact = new Contact(e.Message.Chat.Id, e.Message.Chat.FirstName, e.Message.Chat.LastName);
                
                MessageLogs.Add(new MessageLog(
                    DateTime.Now.ToShortTimeString(),
                    contact,
                    Message));

                if (!IsContainsContact(contact))
                {
                    ContactsList.Add(new Contact(
                        e.Message.Chat.Id, 
                        e.Message.Chat.FirstName, 
                        e.Message.Chat.LastName));
                }
            });
        }

        public void SendMessage(Contact contact, string message)
        {
            _bot.SendTextMessageAsync(contact.Id, message);
            MessageLogs.Add(new MessageLog(
                DateTime.Now.ToShortTimeString(),
                Contact.Empty,
                message
                ));
        }

        public async Task AttachFile(Contact contact, string path)
        {
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await _bot.SendDocumentAsync(contact.Id, new InputOnlineFile(stream, Path.GetFileName(path)));
            }

            MessageLogs.Add(new MessageLog(
                DateTime.Now.ToShortTimeString(),
                Contact.Empty,
                $"File: {Path.GetFileName(path)}"
            ));
        }

        public void Save()
        {
            DataLoader.ExportJson(HystoryPath, MessageLogs);
            DataLoader.ExportJson(ContactsPath, ContactsList);
        }
    }
}
