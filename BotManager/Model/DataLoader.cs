﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace BotManager.Model
{
    public struct DataLoader
    {
        public static void ExportJson(string path, object data)
        {
            string json = JsonConvert.SerializeObject(data);
            File.WriteAllText(path,json);
        }

        public static List<T> ImportJson<T>(string path)
        {
            List<T> result = new List<T>();

            if (!File.Exists(path)) return result;

            string json = File.ReadAllText(path);
            var data = JsonConvert.DeserializeObject<List<T>>(json);

            if(data != null) result.AddRange(data);

            return result;
        }
    }
}