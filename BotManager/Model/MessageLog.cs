﻿using System;

namespace BotManager.Model
{
    public struct MessageLog
    {
        public String Time { get; set; }
        public Contact Contact { get; set; }
        public String Message { get; set; }

        public MessageLog(string time, Contact contact, string message)
        {
            Time = time;
            Contact = contact;
            Message = message;
        }
    }
}