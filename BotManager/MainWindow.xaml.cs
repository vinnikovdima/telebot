﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BotManager.Model;
using Microsoft.Win32;

namespace BotManager
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly TelegramMessageClient _client;

        public MainWindow()
        {
            InitializeComponent();

            _client = new TelegramMessageClient(this);

            logList.ItemsSource = _client.MessageLogs;
            contactList.ItemsSource = _client.ContactsList;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _client.Save();
        }

        private void SendMessage()
        {
            if (contactList.SelectedIndex >= 0 && !String.IsNullOrEmpty(inputMessage.Text))
            {
                _client.SendMessage((Contact)contactList.SelectedItem, inputMessage.Text);
                inputMessage.Text = "";
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            SendMessage();
        }

        private void AttachButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (contactList.SelectedIndex >= 0 && openFileDialog.ShowDialog() == true)
            {
                _client.AttachFile((Contact)contactList.SelectedItem, openFileDialog.FileName);
            }
        }

        private void inputMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                SendMessage();
        }

        private void contactList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            controllsPanel.IsEnabled = true;
        }
    }
}
